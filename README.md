nt (Next Thing)
==========

A command line tool (for integration with git) to track future commits.

What It Is and What It Isn't
----------------------------

nt is for keeping track of those todos that pop up in the middle of a 
coding session (and only having to type them exactly once).

The future plan is to integrate this with various bug trackers (think Trello).


Dependencies
------------

A pure and noble heart.

Installation
------------

Hopefully using pip, brew, or some other package manager.

Setup
-----

Before you can start using nt, you must have a git project to begin tracking.
Setup assumes you are in the root directory of a project.

1. Initialize tn and hook it into git.

   $ nt init .
   $ nt hook

   (You can always unhook with "tn unhook".)

2. Create your first todo.

   On the command line:
   $ nt todo "clean the bizzrablisher"

   With your editor:
   $ nt todo

3. Select something to work on. (For example, the todo we just made.)

   Launch interactive picker:
   $ nt do

   List todos:
   $ nt list todos

   On the command line:
   $ nt do 0

4. Hack hack hack.

   If you are making an interim commit:
   $ git commit <-p|--partial> (normal options)

   If you are done, just commit like normal:
   $ git commit

   The current todo automagically populates in your editor. After the 
   commit, the current todo is moved to the done pile.

5. Repeat steps 2-4 until you have hacked the planet in an orderly fashion.
