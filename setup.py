#!/usr/bin/env python

from distutils.core import setup

setup(name='nt',
      version='0.1',
      description='Next Thing',
      author='David Samuelson', # please add your names when you contribute
      author_email='sralmai@sharetruth.net',
#      url='http://git.digi.com/python/pyidigi',
#      packages=['idigi'],
      scripts=['scripts/nt'],
      )
